import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'annonces',
    pathMatch: 'full'
  },
  {
    path: 'annonces',
    loadChildren: () =>
    import('./pages/annonce-management/annonce-management.module').then(
      m => m.AnnonceManagementModule
    )
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
