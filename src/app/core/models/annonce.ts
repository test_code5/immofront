export class Annonce {

  id?: number;
  title?: any;
  description?: any;
  picture?: any;
  place?: any;
  image?:any;
  createdAt?: Date;
  updateAt?: Date;
  picturePath?: string;

}
