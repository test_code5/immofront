import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Annonce } from '../models/annonce';

@Injectable({
  providedIn: 'root'
})

export class AnnoncementService {

  _url = environment.baseUrl + '/announcements/'

  constructor(
    private _httpClient: HttpClient
  ) { }

  getAllAnnouncements() {

    const headers = new HttpHeaders()
      .append('Content-Type', 'application/json')
      .append('Access-Control-Allow-Headers', 'Content-Type')
      .append('Access-Control-Allow-Methods', '*')
      .append('Access-Control-Allow-Origin', '*');

    return this._httpClient.get(this._url,  {headers});
  }

  getAnnouncementById(annonce: Annonce) {

  }

  findAnnouncementByTitle(annonce: Annonce) {

  }

  searchAnnouncement(annonce: Annonce) {

  }

  searchAnnouncementByPlace(annonce: Annonce) {

  }

  searchAnnouncementByTitle(annonce: Annonce) {

  }

  addAnnouncement(annonce: Annonce) {

    let formData = new FormData()
    formData.append('title', annonce.title)
    formData.append('description', annonce.description)
    formData.append('place', annonce.place)
    formData.append('image', annonce.picture)

    return this._httpClient.post(this._url, formData)
  }

  updateAnnouncement(annonce: Annonce, newImage:string) {

    let formData = new FormData()
    formData.append('title', annonce.title)
    formData.append('description', annonce.description)
    formData.append('place', annonce.place)

    if(newImage) {
      formData.append('image', annonce.picture)
    }

    return this._httpClient.put(this._url + annonce.id, formData)

  }

  deleteAnnouncement(annonce: Annonce) {
    return this._httpClient.delete(this._url + annonce.id)
  }

}
