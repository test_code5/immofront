import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Annonce } from 'src/app/core/models/annonce';
import { AnnoncementService } from 'src/app/core/services/annoncement.service';

@Component({
  selector: 'app-annonce-add',
  templateUrl: './annonce-add.component.html',
  styleUrls: ['./annonce-add.component.scss']
})
export class AnnonceAddComponent implements OnInit {

  imageURL: any;
  file: any;
  announce = new Annonce();
  loader = false;

  constructor(
    public dialogRef: MatDialogRef<AnnonceAddComponent>,
    private announcementServices: AnnoncementService,
		@Inject(MAT_DIALOG_DATA) public data: Annonce,) { }

  ngOnInit(): void {
  }

  fileChangeEvent(event: any) {

    this.file = event.target.files[0];

    const reader = new FileReader()

    reader.readAsDataURL(this.file);

    reader.onload = () => {
      this.imageURL = reader.result;
      this.announce.picture = this.file;
    }

  }

  submit() {

    this.loader = true;

    this.announcementServices.addAnnouncement(this.announce).subscribe(
      (res:any) => {
        this.closeModal()
      },
      (err:any) => {
        console.log(err)
      },
      () => {

      }
    )

  }

  closeModal() {
    this.dialogRef.close()
  }

}
