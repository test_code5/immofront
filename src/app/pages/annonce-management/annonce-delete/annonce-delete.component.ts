import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Annonce } from 'src/app/core/models/annonce';
import { AnnoncementService } from 'src/app/core/services/annoncement.service';

@Component({
  selector: 'app-annonce-delete',
  templateUrl: './annonce-delete.component.html',
  styleUrls: ['./annonce-delete.component.scss']
})
export class AnnonceDeleteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AnnonceDeleteComponent>,
    private announcementServices: AnnoncementService,
		@Inject(MAT_DIALOG_DATA) public data: Annonce,) { }

  ngOnInit(): void {
  }

  closeModal() {
    this.dialogRef.close()
  }

  submit() {

    this.announcementServices.deleteAnnouncement(this.data).subscribe(
      (res:any) => {
        this.closeModal();
        console.log(res);
      },
      (err:any) => {

      },
      () => {

      }

    )

  }

}
