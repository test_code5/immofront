import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Annonce } from 'src/app/core/models/annonce';
import { environment } from 'src/environments/environment.prod';
import { AnnonceDeleteComponent } from '../annonce-delete/annonce-delete.component';
import { AnnonceUpdateComponent } from '../annonce-update/annonce-update.component';

@Component({
  selector: 'app-annonce-details',
  templateUrl: './annonce-details.component.html',
  styleUrls: ['./annonce-details.component.scss']
})
export class AnnonceDetailsComponent implements OnInit {

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<AnnonceDetailsComponent>,
		@Inject(MAT_DIALOG_DATA) public data: Annonce,) { }

  ngOnInit(): void {
    console.log(this.data)
  }

  closeModal() {
    this.dialogRef.close()
  }

  getBaseUrl() {
    return environment.baseUrl;
  }

  deleteAnnouncement() {

    const dialogRef = this.dialog.open(AnnonceDeleteComponent, {
      width: "500px",
      height: "150px",
      data: this.data
    })

    dialogRef.afterClosed().subscribe(res => {
      this.closeModal();
    });

  }

  updateAnnouncement() {

    const dialogRef = this.dialog.open(AnnonceUpdateComponent, {
      width: "60%",
      height: "auto",
      maxHeight: '95%',
      data: this.data
    })

    dialogRef.afterClosed().subscribe(res => {
      this.closeModal();
    });

  }

}
