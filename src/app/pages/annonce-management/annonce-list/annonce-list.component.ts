import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Annonce } from 'src/app/core/models/annonce';
import { AnnoncementService } from 'src/app/core/services/annoncement.service';
import { environment } from 'src/environments/environment.prod';
import { AnnonceAddComponent } from '../annonce-add/annonce-add.component';
import { AnnonceDetailsComponent } from '../annonce-details/annonce-details.component';

@Component({
  selector: 'app-annonce-list',
  templateUrl: './annonce-list.component.html',
  styleUrls: ['./annonce-list.component.scss']
})
export class AnnonceListComponent implements OnInit {

  announcementList : Array<Annonce> = [];
  loader = true;

  constructor(
    public dialog: MatDialog,
    private announcementServices: AnnoncementService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getAllAnnouncement()
  }

  getAllAnnouncement() {

    this.announcementServices.getAllAnnouncements().subscribe(
      (res:any) => {
        console.log(res)
        this.announcementList = res;
      },
      (err:any) => {

      },
      () => {
        this.loader = false;
        this.cdr.markForCheck();
      }
    )

  }

  getBaseUrl() {
    return environment.baseUrl;
  }

  newAnnouncement() {

    const dialogRef = this.dialog.open(AnnonceAddComponent, {
      width: "60%",
      height: "auto",
      maxHeight: '95%'
    })

    dialogRef.afterClosed().subscribe(res => {
      this.ngOnInit();
    });

  }

  announcementDetails(anonce:any) {

    const dialogRef = this.dialog.open(AnnonceDetailsComponent, {
      width: "60%",
      height: "auto",
      maxHeight: '95%',
      data: anonce
    })

    dialogRef.afterClosed().subscribe(res => {
      this.ngOnInit();
    });

  }

}
