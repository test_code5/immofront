import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnnonceListComponent } from './annonce-list/annonce-list.component';
import { AnnonceAddComponent } from './annonce-add/annonce-add.component';
import { AnnonceDetailsComponent } from './annonce-details/annonce-details.component';
import { AnnonceDeleteComponent } from './annonce-delete/annonce-delete.component';
import { RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/core/modules/material.module';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnnonceUpdateComponent } from './annonce-update/annonce-update.component';

@NgModule({
  declarations: [
    AnnonceListComponent,
    AnnonceAddComponent,
    AnnonceDetailsComponent,
    AnnonceDeleteComponent,
    AnnonceUpdateComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatDialogModule,
    FormsModule, ReactiveFormsModule,
    RouterModule.forChild(
      [
        {
          path: '',
          component: AnnonceListComponent
        },
        {
          path: '/:annonce_id',
          component: AnnonceDetailsComponent
        }
      ]
    )
  ],
  providers: [

  ]
})

export class AnnonceManagementModule { }
