import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Annonce } from 'src/app/core/models/annonce';
import { AnnoncementService } from 'src/app/core/services/annoncement.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-annonce-update',
  templateUrl: './annonce-update.component.html',
  styleUrls: ['./annonce-update.component.scss']
})
export class AnnonceUpdateComponent implements OnInit {

  imageURL: any;
  file: any;
  announce = new Annonce();
  loader = false;

  constructor(
    public dialogRef: MatDialogRef<AnnonceUpdateComponent>,
    private announcementServices: AnnoncementService,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.announce = this.data;
    console.log(this.data)
  }

  getBaseUrl() {
    return environment.baseUrl;
  }

  fileChangeEvent(event: any) {

    this.file = event.target.files[0];

    const reader = new FileReader()

    reader.readAsDataURL(this.file);

    reader.onload = () => {
      this.imageURL = reader.result;
      this.announce.picture = this.file;
    }

  }

  submit() {

    this.loader = true;

    this.announcementServices.updateAnnouncement(this.announce, this.imageURL).subscribe(
      (res:any) => {
        console.log(res);
        this.closeModal()
      },
      (err:any) => {
        console.log(err)
      },
      () => {

      }
    )

  }

  closeModal() {
    this.dialogRef.close()
  }

}
